from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import Http404
from django.shortcuts import HttpResponse
import json
from django.shortcuts import HttpResponseRedirect
from django.views.generic import TemplateView
from django.views.generic import View
from time import time
from hashlib import md5
from django.conf import settings
import os


class MainView(TemplateView):
    template_name = "catalog_client/main.html"


class BaseView(TemplateView):
    template_name = "catalog_client/authors.html"
    def get_context_data(self, **kwargs):
        ctx = super(BaseView, self).get_context_data(**kwargs)
        if self.type: ctx['type'] = self.type
        return ctx
 

class AuthorsView(BaseView):
    type = 'authors'


class SectionsView(BaseView):
    type = 'sections'


class BooksView(BaseView):
    type = 'books'


class TestView(View):
    def get(self, request, *args, **kwargs):
        body = None
        if 'filepath' in request.GET:
            body = { 'file':request.GET['filepath'] }
        else:
            raise Http404
        return HttpResponse(json.dumps(body), content_type="application/json")

    def handle_uploaded_file(self, f):
        # Generate unique name.
        name = md5((f.name+str(time())).encode('utf-8')).hexdigest()
        extension = f.name[len(f.name)-4:len(f.name)]
        name+=extension
        
        # Check existance of directory and create if need.
        APP_NAME = 'catalog'
        MODEL_NAME = 'book'
        path = os.path.join(settings.MEDIA_ROOT, APP_NAME, MODEL_NAME, name[:2], name[2:4]) 
        if not os.path.exists(path):
            os.makedirs(path)

        # Write file.
        filepath = os.path.join(path, name)
        with open(filepath, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

        # Create relative path for response.
        n = filepath.find(APP_NAME)
        filepath = filepath[n:len(filepath)]
        return filepath

    def post(self, request, *args, **kwargs):
        file_obj = request.FILES['file']
        filepath = self.handle_uploaded_file(file_obj)
        return HttpResponseRedirect('/test/?filepath=%s' % filepath)
