from django.conf.urls import patterns, url
from .views import AuthorsView
from .views import BooksView
from .views import SectionsView
from .views import TestView
from .views import MainView

urlpatterns = patterns('',
    url(r'^books/$', BooksView.as_view(), name='books'),
    url(r'^sections/$', SectionsView.as_view(), name='sections'),
    url(r'^authors/$', AuthorsView.as_view(), name='authors'),
    url(r'^$', MainView.as_view(), name='index'),
    url(r'^test/$', TestView.as_view()),
)
