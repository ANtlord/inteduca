from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import patterns
from django.conf.urls import include
from .views import CreateBookAPIView
from .views import BookListAPIView
from .views import BookAPIView

from .views import SectionAPIView
from .views import CreateSectionAPIView
from .views import SectionListAPIView

from .views import CreateAuthorAPIView
from .views import AuthorAPIView
from .views import AuthorListAPIView

from .views import SectionBookListAPIView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = patterns('',
    url(r'authors/$', csrf_exempt(AuthorListAPIView.as_view()), name='author-list'),
    url(r'authors/(?P<pk>[0-9]+)/$', csrf_exempt(AuthorAPIView.as_view()), name='author-detail'),
    url(r'authors/new/$', CreateAuthorAPIView.as_view()),

    url(r'sections/$', csrf_exempt(SectionListAPIView.as_view()), name='section-list'),
    url(r'sections/(?P<pk>[0-9]+)/$', csrf_exempt(SectionAPIView.as_view()), name='section-detail'),
    url(r'sections/new/$', csrf_exempt(CreateSectionAPIView.as_view())),

    url(r'books/$', csrf_exempt(BookListAPIView.as_view()), name='book-list'),
    url(r'books/(?P<pk>[0-9]+)/$', csrf_exempt(BookAPIView.as_view()), name='book-detail'),
    url(r'books/new/$', csrf_exempt(CreateBookAPIView.as_view())),

    url(r'books-section/(?P<pk>[0-9]+)/$', csrf_exempt(SectionBookListAPIView.as_view()), name='book-list'),
    url(r'api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
urlpatterns = format_suffix_patterns(urlpatterns)
