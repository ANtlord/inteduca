from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.generics import CreateAPIView
from .serializers import BookSerializer, SectionSerializer
from .serializers import AuthorSerializer
from .models import Book
from .models import Author
from .models import Section


# SECTIONS VIEWS.
class SectionAPIView(RetrieveUpdateDestroyAPIView):
    model = Section
    serializer_class = SectionSerializer


class CreateSectionAPIView(CreateAPIView):
    model = Section
    serializer_class = SectionSerializer


class SectionListAPIView(ListAPIView):
    model = Section
    serializer_class = SectionSerializer


# BOOKS VIEWS.
class CreateBookAPIView(CreateAPIView):
    model = Book
    serializer_class = BookSerializer


class BookListAPIView(ListAPIView):
    model = Book
    serializer_class = BookSerializer


class BookAPIView(RetrieveUpdateDestroyAPIView):
    model = Book
    serializer_class = BookSerializer


# AUTHORS VIEWS.
class CreateAuthorAPIView(CreateAPIView):
    model = Author
    serializer_class = AuthorSerializer


class AuthorListAPIView(ListAPIView):
    model = Author
    serializer_class = AuthorSerializer


class AuthorAPIView(RetrieveUpdateDestroyAPIView):
    model = Author
    serializer_class = AuthorSerializer


class SectionBookListAPIView(BookListAPIView):
    def get(self, request, pk, *args, **kwargs):
        self.section_id = pk
        return super(SectionBookListAPIView, self).get(request, *args, **kwargs)
    
    def get_queryset(self, **kwargs):
        qs = super(SectionBookListAPIView, self).get_queryset(**kwargs)
        qs = qs.filter(section_id=self.section_id)
        return qs
