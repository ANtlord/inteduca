from django.db import models
from hashlib import md5
from os import path as op
from time import time
from django.core.urlresolvers import reverse

class NameModel(models.Model):
    class Meta:
        abstract=True

    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


def generate_upload_name(instance, filename, prefix=None, unique=False):
    """
    Auto generate name for File and Image fields.
    """
    ext = op.splitext(filename)[1]
    name = str(instance.pk or '') + filename + (str(time()) if unique else '')

    # We think that we use utf8 based OS file system
    filename = md5(name.encode('utf8')).hexdigest() + ext
    basedir = op.join(instance._meta.app_label, instance._meta.module_name)
    if prefix:
        basedir = op.join(basedir, prefix)
    return op.join(basedir, filename[:2], filename[2:4], filename)


class Section(NameModel):
    """Model represents instance of section of books"""
    class Meta:
        verbose_name = u'section'
        verbose_name_plural = u'sections'
    
    def get_absolute_url(self):
        return reverse('section-detail', kwargs={'pk': self.pk})


class Book(NameModel):
    """Model represents instance of book"""
    class Meta:
        verbose_name = u'Book'
        verbose_name_plural = u'Books'

    short_text = models.CharField(max_length=255, blank=True)
    image = models.ImageField(upload_to=generate_upload_name, blank=True, null=True)
    section = models.ForeignKey(Section, related_name='books')
    authors = models.ManyToManyField('Author', related_name='books')

    def get_absolute_url(self):
        return reverse('book-detail', kwargs={'pk': self.pk})


class Author(NameModel):
    """Model represents instance of author"""
    class Meta:
        verbose_name = u'Author'
        verbose_name_plural = u'Authors'

    def get_absolute_url(self):
        return reverse('author-detail', kwargs={'pk': self.pk})
