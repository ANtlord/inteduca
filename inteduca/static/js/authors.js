function getParam (URL, sname)
{
    if (URL){
        var params = URL.substr(URL.indexOf("?")+1);
        var sval = "";
        params = params.split("&");
        // split param and value into individual pieces
        for (var i=0; i<params.length; i++) {
            temp = params[i].split("=");
            if ( [temp[0]] == sname ) { sval = temp[1]; }
        }
        return sval;
    }
    return null;
}

var app = angular.module('authorsApp', ['ngRoute']);
var type = $('.data-block').data('type');

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.config(['$routeProvider',
    function($routeProvider) {
        var prefix = "";
        if (type == "books") prefix = "books_";

        $routeProvider.
            when('/new', {
                templateUrl: '/static/html/'+prefix+'detail.html',
                controller: 'NewCtrl'
            }).
            when('/:page', {
                templateUrl: '/static/html/list.html',
                controller: 'ListCtrl'
            }).
            when('/edit/:ID', {
                templateUrl: '/static/html/'+prefix+'detail.html',
                controller: 'EditCtrl'
            }).
            otherwise({
                redirectTo: '/1/'
            });
}]);


app.controller('ListCtrl', function($scope, $routeParams, $http){
    $scope.items = [];
    $http.get('/api/'+type+'/?page='+$routeParams.page).then(function(result){
        for (var i = 0; i < result.data.results.length; i++) {
            $scope.items.push( result.data.results[i] );
        }
        if ('previous' in result.data && result.data.previous != null) {
            var qs = result.data.previous;
            $scope.prev = getParam(qs, 'page');
        }
        if ('next' in result.data && result.data.next != null) {
            var qs = result.data.next;
            $scope.next = getParam(qs, 'page');
        }
    });
});

function detailControl($scope, item, $http, $location, _method, _url){
    /*
     * Method for saving authors to current item.
     */
    $scope.authorsChanged = function() {
        item.authors = [];
        for (var i = 0; i < $scope.currentAuthors.length; i++) {
            item.authors.push($scope.currentAuthors[i].id);
        }
    }

    /*
     * Method for saving section of book.
     */
    $scope.sectionChanged = function() {
        item.section = $scope.currentSection.id;
    }

    /*
     * Saving item.
     */
    $scope.save = function(){
        var file = $scope.file;
        var fd = new FormData();

        // Add image to item properties.
        if (type == 'books') {
            if (file != null) $scope.item.image = file;
            else delete $scope.item['image'];
        }
        // Move all item data to form data.
        for(key in $scope.item){
            if ($scope.item[key] instanceof Array) {
                for (var i = 0; i < $scope.item[key].length; ++i) {
                    fd.append(key, $scope.item[key][i]);
                }
            }
            else fd.append(key, $scope.item[key]);
        }

        $http({
            method: _method,
            url: _url,
            headers: {
                'Content-Type': undefined
            },
            data: fd,
            transformRequest: angular.identity
        }).then(
            function(result){
                $location.path('/');
            }
        );
    }
    
    /*
     * Deletion instance.
     */
    $scope.delete = function(){
        $http.delete(_url).then(function(){
            $location.path('/');
        });
    }
}

app.controller('NewCtrl', function($scope, $location, $http){
    $http.get('/api/'+type+'/').then(function(result){
        if (type=="books") {
            $scope.sections = [];
            $http.get('/api/sections/').then(function(result){
                for (var i = 0; i < result.data.results.length; i++) {
                    $scope.sections.push( result.data.results[i] );
                }
            });

            $scope.authors = [];
            $http.get('/api/authors/?page_size=100').then(function(result){
                for (var i = 0; i < result.data.results.length; i++) {
                    $scope.authors.push( result.data.results[i] );
                }
            });
        }
    });
    $scope.item = {}
    detailControl($scope, $scope.item, $http, $location, 'POST',
                  '/api/'+type+'/new/');
});

app.controller('EditCtrl', function($scope, $routeParams, $location, $http){
    $http.get('/api/'+type+'/'+$routeParams.ID+'/').then(function(result){
        $scope.item = result.data;
        if (type=="books") {
            $scope.sections = [];

            $http.get('/api/sections/').then(function(result){
                for (var i = 0; i < result.data.results.length; i++) {
                    $scope.sections.push( result.data.results[i] );
                    if ( result.data.results[i].id == $scope.item.section ) {
                        $scope.currentSection = result.data.results[i];
                    }
                }
            });

            $scope.currentAuthors = [];
            $scope.authors = [];
            $http.get('/api/authors/?page_size=100').then(function(result){
                for (var i = 0; i < result.data.results.length; i++) {
                    $scope.authors.push( result.data.results[i] );
                    if ( $scope.item.authors.indexOf(result.data.results[i].id) != -1 ) {
                        $scope.currentAuthors.push(result.data.results[i]);
                    }
                }
            });
        }
        detailControl($scope, $scope.item, $http, $location, 'PUT',
                      '/api/'+type+'/'+$routeParams.ID+'/');
    });
});
